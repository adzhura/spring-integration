package com.otus.spring.integration;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.otus.spring.integration.domain.Event;
import com.otus.spring.integration.domain.EventPack;
import com.otus.spring.integration.service.ItgService;


@SpringBootApplication
public class ItgApp implements CommandLineRunner
{
    @Autowired
    private PublishSubscribeChannel outputChannel;
    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    private ExecutorService threadPool;

    @Autowired
    private ItgService itgService;

    public static void main(String[] args) throws Exception
    {
        SpringApplication.run(ItgApp.class, args);
    }

    @Override
    public void run(String... args) throws Exception
    {
        int inputCount = 200;
        AtomicInteger outputCount = new AtomicInteger();
        long startTime = System.currentTimeMillis();

        outputChannel.subscribe(m -> {
            EventPack eventPack = (EventPack) m.getPayload();
            outputCount.addAndGet(eventPack.getEvents().size());
            System.out.println(eventPack);
        });

        try
        {
            for (int i = 0; i < inputCount; i++)
            {
                itgService.handle(new Event(i, "name" + i));

                Thread.sleep(100);
            }
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        try
        {
            Thread.sleep(1500);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        try
        {
            taskScheduler.shutdown();
        }
        catch (Throwable t)
        {
            System.out.println("Interrupted");
        }

        threadPool.shutdown();

        while (!threadPool.isTerminated())
        {
            Thread.sleep(1000);
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Output count: " + outputCount.get());
        System.out.println("Speed: "
            + ((double) outputCount.get()) / (endTime - startTime) * 1000
            + " obj/sec");
    }
}
