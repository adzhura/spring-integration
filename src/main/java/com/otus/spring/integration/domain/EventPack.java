package com.otus.spring.integration.domain;


import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class EventPack
{
    private List<Event> events;
}
