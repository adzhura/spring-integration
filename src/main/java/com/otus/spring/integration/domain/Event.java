package com.otus.spring.integration.domain;


import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class Event
{
    private int index;
    private String msg;
}
