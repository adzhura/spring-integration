package com.otus.spring.integration.service;


import java.util.function.Function;

import org.springframework.stereotype.Service;

import com.otus.spring.integration.domain.Event;

@Service
public class EventHandler implements Function<Event, Event>
{
    @Override
    public Event apply(Event e)
    {
        System.out.println(Thread.currentThread().getName() +  ": begin -> " + e);
        
        try
        {
            Thread.sleep(1000);
        }
        catch (InterruptedException e1)
        {
            e1.printStackTrace();
        }
        
        Event e2 = new Event(e.getIndex(), e.getMsg().toUpperCase());

        System.out.println(Thread.currentThread().getName() +  ": end -> " + e);
        
        return e2;
    }
}
