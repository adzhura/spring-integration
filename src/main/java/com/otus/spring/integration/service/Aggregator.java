package com.otus.spring.integration.service;


import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.integration.aggregator.AbstractAggregatingMessageGroupProcessor;
import org.springframework.integration.store.MessageGroup;
import org.springframework.stereotype.Component;

import com.otus.spring.integration.domain.Event;


@Component
public class Aggregator extends AbstractAggregatingMessageGroupProcessor
{
    @Override
    protected Object aggregatePayloads(MessageGroup group,
        Map<String, Object> defaultHeaders)
    {
        return group.getMessages().stream().map(m -> (Event) m.getPayload())
                .collect(Collectors.toList());
    }
}
