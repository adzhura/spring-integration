package com.otus.spring.integration.service;


import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

import com.otus.spring.integration.domain.Event;


@MessagingGateway
public interface ItgService
{
    @Gateway(requestChannel = "inputChannel")
    void handle(Event e);
}