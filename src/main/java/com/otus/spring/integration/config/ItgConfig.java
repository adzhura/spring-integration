package com.otus.spring.integration.config;


import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.channel.MessageChannels;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.otus.spring.integration.domain.Event;
import com.otus.spring.integration.domain.EventPack;
import com.otus.spring.integration.service.Aggregator;
import com.otus.spring.integration.service.EventHandler;


@Configuration
public class ItgConfig
{
    @Bean
    public ThreadPoolTaskScheduler taskScheduler()
    {
        ThreadPoolTaskScheduler threadPool = new ThreadPoolTaskScheduler();
        threadPool.setPoolSize(
            Math.max(Runtime.getRuntime().availableProcessors(), 2));
        return threadPool;
    }

    @Bean
    public ExecutorService threadPool()
    {
        return Executors.newFixedThreadPool(
            Math.max(Runtime.getRuntime().availableProcessors(), 2));
    }

    @Bean
    public IntegrationFlow eventFlow(PublishSubscribeChannel inputChannel,
        PublishSubscribeChannel outputChannel, Aggregator aggregator,
        ThreadPoolTaskScheduler taskScheduler, ExecutorService threadPool,
        EventHandler eventHandler)
    {
        return IntegrationFlows.from(inputChannel) // ---------------------
            .log() // ----------------------------
            .aggregate(a -> a
                .correlationStrategy(
                    m -> Long.toString(m.getHeaders().getTimestamp() / 1000))
                .releaseStrategy(g -> {
                    return g.size() > 2;
                }).sendPartialResultOnExpiry(true).groupTimeout(1000)
                .expireGroupsUponCompletion(true).expireGroupsUponTimeout(true)
                .taskScheduler(taskScheduler).outputProcessor(aggregator))
            .log() // ---------------------------------------
            .split() // --------------------------------------
            .log() // -----------------------------------------------
            .channel(MessageChannels.executor(threadPool))
            .transform(eventHandler, "apply") // ----------------
            .aggregate()
            .<List<Event>, EventPack> transform(m -> new EventPack(m))
            .channel(outputChannel).get();
    }

    @Bean
    public PublishSubscribeChannel inputChannel()
    {
        return MessageChannels.publishSubscribe().get();
    }

    @Bean
    public PublishSubscribeChannel outputChannel()
    {
        return MessageChannels.publishSubscribe().get();
    }
}
